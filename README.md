## Instructions
1. Run docker compose file as is `docker compose up -d`
2. execute `docker exec -it runner1 /bin/bash`
3. once in the container shell `gitlab-runner register` (select **shell** when asked)
4. fill the data provided by gitlab (settings>ci_cd>New project runner)
5. exit container shell `exit`
6. stop the docker compose `docker compose down`
7. edit the docker compose commenting the line *command* as following `nano docker-compose.yaml`
```
    #command: 'tail -f /dev/null'
```
8. start the docker again `docker compose up -d`

> edit image and container_name as you prefer.

